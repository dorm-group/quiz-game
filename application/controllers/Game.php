<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

class Game extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        //$this->main_page();
        $this->main_page();
    }

    public function new_quickplay(){
        
        $numbers = range(1, 10);
        shuffle($numbers);
        $json=json_encode($numbers);
        $this->tpl->assign('order',$json);
        $this->tpl->show('game.tpl');
    }

    public function laderboard(){
        $data = $this->input->post();
        $name=$data[0];
        $score=$data[1];
        $this->load->model('play_model');
        $this->play_model->upload_laderboard($name,$score);
        $laderboard=$this->play_model->download_laderboard();
        $laderboard=obj2arr($laderboard);
        
        $this->tpl->assign('laderboard', $laderboard);
        $this->tpl->show('scores.tpl');
    }






    public function get_question(){
        $ajax = $this->input->is_ajax_request();
        if ($ajax) {
            $id = $this->input->post("id");
            $this->load->model('play_model');
            $datas=$this->play_model->get_datas($id);
            $return=json_encode($datas);
            
            print_r($return);
            exit();
        }
        else{
            die();
        }
    }


    public function main_page(){
        //phpinfo();
        //$this->database
        $this->tpl->show('main_page.tpl');
        
    }

    public function loading(){
        $this->tpl->show('loading.tpl');

    }

    public function pdf_select_view()
    {
        $documents = $this->pdf->get_available_documents();
        if ($documents === false) {
            set_flash("Nincs megnyitható dokumentum", 'danger');
        }
        $this->tpl->assign('pdfs', $documents);
        $this->tpl->show('pdf_selector.tpl');
    }

}