<html> 
    <head>
        <title>Quizgame</title>
        <link rel="stylesheet" href="{$base_url}css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

        <script defer src="https://use.fontawesome.com/releases/v5.8.1/js/all.js" integrity="sha384-g5uSoOSBd7KkhAMlnQILrecXvzst9TdC09/VM+pjDTCM+1il8RHz5fKANTFFb+gQ" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.4.1.js" type="text/javascript"></script>
        <script src="{$base_url}js/jquery.redirect.js"></script>
        <script src="{$base_url}js/popper.min.js" type="text/javascript"></script>
        <script src="{$base_url}js/bootstrap.min.js" type="text/javascript"></script>

    <!-- [SAJÁT]- CSS -->
        <link rel="stylesheet" href="{$base_url}css/mystyle.css">
        <link rel="stylesheet" href="{$base_url}css/mystyle_game.css">
    <!-- [SAJÁT] - JS -->
        <script src="{$base_url}js/myscript.js"></script>
    </head>
        <header>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark mynavbar">
            <div class="container">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample07" aria-controls="navbarsExample07" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
        
                <div class="navbar-collapse collapse " id="navbarsExample07">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active ">
                    <a class="nav-link" href="{$base_url}">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="#">About us</a>
                    </li>
                </ul>
                
                <div class="dropdown">
                    <button class="nav-link dropdown-toggle btn-success btn"  id="signin-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Sign in</button>
                    <div class="dropdown-menu fadeIn" aria-labelledby="signin-dropdown">
                        <form class="form-group dropdown-signin" id="signinform" action="">
                            <label  for="email">e-mail address</label>
                            <input class="form-control textbox" id="email" type="email" required>
                            <label for="pass">password</label>
                            <input class="form-control textbox" id="pass" type="password" required>
                            <br>
                            <input class="btn btn-success" id="signin" type="button" value="Sign in">
                            <input class="btn btn-dark" id="reg" type="button" value="Register">
                        </form>
                    </div>
                </div>
                </div>
            </div>
        </nav>
    </header> 