{include file="head.tpl"}
    <br>
    <body>
        <div class="container">
        <div class="row">
            <div class="col"></div>
            <div class="col-md-4">
                <form class="form-group text-center" id="joinform" action="">
                    <label id="pin-label" for="pin">Enter PIN:</label>
                    <input class="form-control" id="pin" type="text" required>
                    <input class="btn btn-success" id="join" type="button" value="JOIN">
                </form>
                
            </div>
            <div class="col"></div>
        </div>


        <div class="row justify-content-center">
            
            <div class="mx-auto ">
                <button  class="card text-center align-items-center justify-content-center" id="qplay">
                    <i class="fas fa-bolt card-logo"></i>
                    <div class="card-body card-logo-color text-center">
                        <label class="maintext">Quick play</label>
                        <div class="d-flex justify-content-between">
                            <label class="description">In this game mode you can pick a specific subject and play 
                                with 10 random people. To play this mode you don't 
                                have to sign in.</label>
                        </div>
                    </div>
                </button>
            </div>
                <div class="mx-auto ">
                <div class="inactive-card mb-4 shadow-sm text-center align-items-center justify-content-center">
                    <i class="fas fa-pencil-alt card-logo"></i>
                    <div class="card-body card-logo-color text-center">
                        <label class="maintext">Custom game</label>
                        <div class="d-flex justify-content-between">
                            <label class="description">This game mode allows you to host a game, and generate a
                                 PIN. After the PIN is generated, the clienst can join this
                                 game session. In custom game you'll be able to use your
                                 own custom made questions.</label>
                        </div>
                    </div>
                </div>
            </div>
                <div class="mx-auto">
                <div class="inactive-card mb-4 shadow-sm text-center align-items-center justify-content-center">
                    <i class="fas fa-crown card-logo"></i>
                    
                    <div class="card-body card-logo-color text-center">
                        <label class="maintext">Ranked</label>
                        <div class="d-flex justify-content-between">
                            <label class="description">Ranked means you can play with the modern polihistors.
                                You can compete within all subjects. We use our own MMR system, to learn more, open the "About us"
                                menu above.
                            </label>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
            
        </div>
    </div>

<script>
$('#qplay').click(function(){
    //átirányítást végez ha megnyomjuk a qplay id-val rendelkező gombot.
   window.location.replace('{$base_url}' +'game/new_quickplay');
})

</script>
{include file="footer.tpl"}