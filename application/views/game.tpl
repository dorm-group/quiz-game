{include file="head.tpl"}
    <br>
    <body>

        <div class="container">
            <div class="row">
                <div class="mx-1"></div>
                    <div class="mx-lg-auto" style="width: 100%;">
                        <div class="question-base  text-center align-items-center justify-content-center">
                            <form class="form-group" >
                                <div class="row question justify-content-center">
                                        <Label class="question" id="q" data-order="{$order}">Ide jön majd valami kérdés, hogy ki háyn éves, milyen magas stb.blbjfbsjdbfjsdbfbsdbfbskdfjsbdjkfsdjfb</Label>
                                </div>
                                <div class="timer-container">
                                    <i class="fas fa-clock" style="font-size:50px;"></i>
                                    <p id="time-remaining">6</p>
                                    <div class="timer"></div>
                                </div>
                                <br>
                                <br>
                                <div class="row">
                                    <div class="col">
                                        <input class="mx-auto text-center answer" type="button" name="valasz" id="a" value="asd" onclick='mark("a")'>
                                    </div>
                                    <div class="col">
                                        <input class="mx-auto text-center answer" type="button" name="valasz" id="b" value="b" onclick='mark("b")'>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <input class="mx-auto text-center answer" type="button" name="valasz" id="c" value="c" onclick='mark("c")'>
                                    </div>
                                    <div class="col">
                                        <input class="mx-auto text-center answer" type="button" name="valasz" id="d" value="d" onclick='mark("d")'>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                <div class="mx-1"></div>
            </div>
        </div>

<div class="modal fade" id="inputmodal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" style="color:black;" id="exampleModalScrollableTitle">Kérlek add meg a neved</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <label for="nameinput">Név</label>
        <input type="text" class="form-control" id="nameinput"  placeholder="Név">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick='send_datas()'>ok</button>
      </div>
    </div>
  </div>
</div>

    </body>
    <footer>
        <script>
            var timeleft = 5;
            var timerId = setInterval(countdown, 1000);
            var current_question_id=0;
            var question_order = JSON.parse($("#q").attr("data-order"));
            var question_array=new_question(current_question_id);
            
            
            var current_correct_answer="";
            var correct_answers = 0;
            var base_url='{base_url()}';
            var marked_answer="";

            function countdown() {
                if (timeleft <= -1) {
                    check_answers(marked_answer);

                    alert("A helyes válasz: " + current_correct_answer);
                    reset_buttons();
                    if(current_question_id>9){
                    clearTimeout(timerId);
                    $('#inputmodal').modal();
                    }
                    else{
                        new_question(current_question_id);
                        clearTimeout(timerId);
                        timeleft=10;
                        timerId = setInterval(countdown, 1000);
                    }
                }
                else {
                    $("#time-remaining").text(timeleft);
                    timeleft--;
                }
            }
            
            function send_datas(){
                var name=$("#nameinput").val();
                console.log(name);
                var values=[name, correct_answers];
                var send = JSON.stringify(values);
                $.redirect('laderboard',values);
            }

            function check_answers(marked_answer){
                var marked_question=$("#"+marked_answer).val();
                if(marked_question == current_correct_answer){
                    correct_answers++;
                    console.log(correct_answers);
                    $("#"+marked_answer).removeClass("answer-marked");
                    $("#"+marked_answer).addClass("answer-good");
                }
                else if(marked_answer==""){

                }
                else{
                    $("#"+marked_answer).removeClass("answer-marked");
                    $("#"+marked_answer).addClass("answer-bad");
                }
                marked_answer="";
            }

            function reset_buttons(){
                $("#a").prop('disabled', false);
                $("#b").prop('disabled', false);
                $("#c").prop('disabled', false);
                $("#d").prop('disabled', false);
                $("#a").removeClass("answer-marked");
                $("#b").removeClass("answer-marked");
                $("#c").removeClass("answer-marked");
                $("#d").removeClass("answer-marked");
                $("#a").removeClass("answer-good");
                $("#b").removeClass("answer-good");
                $("#c").removeClass("answer-good");
                $("#d").removeClass("answer-good");
                $("#a").removeClass("answer-bad");
                $("#b").removeClass("answer-bad");
                $("#c").removeClass("answer-bad");
                $("#d").removeClass("answer-bad");
                $("#a").addClass("answer");
                $("#b").addClass("answer");
                $("#c").addClass("answer");
                $("#d").addClass("answer");
            }

            function mark(marked){
                marked_answer=marked;
                if(marked == "a"){
                    $("#a").removeClass("answer");
                    $("#a").addClass("answer-marked");
                }
                else if(marked == "b"){
                    $("#b").removeClass("answer");
                    $("#b").addClass("answer-marked");
                }
                else if(marked == "c"){
                    $("#c").removeClass("answer");
                    $("#c").addClass("answer-marked");
                }
                else if(marked == "d"){
                    $("#d").removeClass("answer");
                    $("#d").addClass("answer-marked");
                }
                $("#a").prop('disabled', true);
                $("#b").prop('disabled', true);
                $("#c").prop('disabled', true);
                $("#d").prop('disabled', true);
                timeleft=0;
            }


            function new_question(id) {
               // $.each(order, function(value) {
                $.ajax({
                        url: "{base_url()}" + 'game/get_question',
                        type: 'POST',
                        data: { id : question_order[id] },
                        dataType: 'json',
                        error: function (xhr, status, error) {
                            console.log(xhr, status, error);
                        },
                        success: function (html) {
                            //alert(JSON.parse(html));
                            var parsed=html;
                            var answers = [parsed[0]["answer1"],parsed[0]["answer2"],parsed[0]["answer3"],parsed[0]["answer4"]] 

                            current_correct_answer=parsed[0]["correctanswer"];
                            console.log(answers);
                            shuffleArray(answers);

                            $("#q").text(parsed[0]["question"]);
                            $("#a").val(answers[0]);
                            $("#b").val(answers[1]);
                            $("#c").val(answers[2]);
                            $("#d").val(answers[3]);
                            current_question_id++;
                        }
                    });
              //  })
            }

            function shuffleArray(array) {
                for (var i = array.length - 1; i > 0; i--) {
                    var j = Math.floor(Math.random() * (i + 1));
                    var temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }


        </script>
{include file="footer.tpl"}
