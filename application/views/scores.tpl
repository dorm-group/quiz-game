{include file="head.tpl"}
    <br>
    <body>

        <div class="container">
            <div class="row">
                <div class="mx-1"></div>
                <div class="mx-lg-auto">
                    <div class="question-base  text-center align-items-center justify-content-center">
                        <form class="table" action="">
                            <div class="row scoreboard">
                                <div class="col">
                                    <br>
                                    {foreach $laderboard as $name}
                                        <p id="st1">{$name["name"]}</p>
                                        <hr>
                                    {/foreach}
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="mx-1"></div>
            </div>
        </div>
{include file="footer.tpl"}
