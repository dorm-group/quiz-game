<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once FCPATH . 'vendor/autoload.php';

class Tpl extends Smarty
{

    private $ci;
    private $template;
    private $template_params;

    public function __construct()
    {
        parent::__construct();

        $this->ci = &get_instance();
        $this->ci->config->load('tpl.php');

        $this->template_params = [
            'pre_css' => [],
            'post_css' => [],
            'pre_js' => [],
            'post_js' => [],
        ];

        $template_dir = $this->ci->config->item('smarty_template_dir');
        $cache_dir = $this->ci->config->item('smarty_cache_dir');
        $compile_dir = $this->ci->config->item('smarty_compile_dir');

        $this->setTemplateDir(!empty($template_dir) ? $template_dir : APPPATH . 'views/');
        $this->setCacheDir(!empty($cache_dir) ? $cache_dir : APPPATH . 'cache/');
        $this->setCompileDir(!empty($compile_dir) ? $compile_dir : APPPATH . 'cache/smarty/');
    }

    public function set_param($type, $value)
    {
        if (in_array($type, array_keys($this->template_params))) {
            $this->template_params[$type] = $value;
        }
    }

    public function add_param($type, $value)
    {
        if (in_array($type, array_keys($this->template_params))) {
            $this->template_params[$type][] = $value;
        }
    }

    private function set_params()
    {
        $this->assign('active_class', $this->ci->router->class);
        $this->assign('active_method', $this->ci->router->method);
        $this->assign('base_url', base_url());
        $this->assignByRef('global', $this->ci->global_model);
        foreach ($this->template_params as $name => $param) {
            $this->assign($name, $param);
        }
    }

    public function view($resource_name, $params = [])
    {
        if (strpos($resource_name, '.') === false) {
            $resource_name .= '.tpl';
        }
        if (is_array($params) && count($params)) {
            foreach ($params as $key => $value) {
                $this->assign($key, $value);
            }
        }

        $tpl_exists = true;
        foreach ($this->template_dir as $tpl_dir) {
            if (!is_file($tpl_dir . $resource_name)) {
                $tpl_exists = false;
            }
        }
        if (!$tpl_exists) {
            show_error('Template: [<em>' . $resource_name . '</em>] cannot be found in <strong>' . implode(', ', $this->template_dir) . '</strong>!');

            return false;
        }

        $this->set_params();

        return parent::display($resource_name);
    }

    public function set_template($template)
    {
        $this->template = $template;
    }

    public function show_template($template = false)
    {
        if ($template) {
            $this->set_template($template);
        }
        $this->set_params();

        return $this->fetch($this->template);
    }

    public function show($template = false)
    {
        if ($template) {
            $this->set_template($template);
        }

        $this->ci->output->set_output($this->show_template());
    }

}
