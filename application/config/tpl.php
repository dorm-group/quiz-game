<?php
	if (!defined('BASEPATH')) {
		exit('No direct script access allowed');
	}

	$config['smarty_template_dir'] = APPPATH . 'views/';
	$config['smarty_compile_dir'] = APPPATH . 'cache/smarty/';
	$config['smarty_cache_dir'] = APPPATH . 'cache/smarty/';
