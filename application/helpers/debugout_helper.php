<?php
	if (!defined('BASEPATH')) {
		exit('No direct script access allowed');
    }
    
    if (!function_exists('debugout')) {
        // die("debugout belép");
		/**
         * Prints out almost any variables. This function only used for debugging
         * @param the variable you want to check
         * @param bool
         */
        function debugout($value, $autodump = false)
        {
            if ($autodump || (empty($value) && $value !== 0 && $value !== "0") || $value === true) {
                var_dump($value);
            } else {
                print_r($value);
            }
            die();
        }

	}