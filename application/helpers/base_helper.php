<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('base_url')) {
    function base_url()
    {
        return 'http://' . $_SERVER['HTTP_HOST'] . '/quiz-game/';
    }
}
