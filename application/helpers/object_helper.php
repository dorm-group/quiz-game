<?php
	if (!defined('BASEPATH')) {
		exit('No direct script access allowed');
    }
    
    if (!function_exists('arr2obj')) {

		function arr2obj($array)
        {
            if (!is_array($array)) {
                return null;
            }
            return (object) $array;
        }
    }

    if (!function_exists('obj2arr')) {
        function obj2arr($obj)
        {
            $_arr = is_object($obj) ? get_object_vars($obj) : $obj;
            foreach ($_arr as $key => $val) {
                $val = (is_array($val) || is_object($val)) ? obj2arr($val) : $val;
                $arr[$key] = $val;
            }
            return $arr;
        }
    }