<?php
	if (!defined('BASEPATH')) {
		exit('No direct script access allowed');
	}

	// Returns true if given bit is set
	if (!function_exists('session_var')) {

		function session_var($name, $default = false, $array = false) {
			if (isset($_SESSION[$name])) {
				$value = $_SESSION[$name];
			}
			else {
				$value = $default;
			}

			if ($array && !is_array($value)) {
				$value = $default;
			}

			return $value;
		}

	}
