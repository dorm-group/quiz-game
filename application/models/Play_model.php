<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

class Play_model extends CI_Model {

public function get_datas($id)
    {
        $this->load->database();
        $this->db->select('');
        $this->db->where('id', $id);
        $this->db->from('questions');
        $documents = $this->db->get()->result();
        $this->db->close();
        return $documents;
    }
public function upload_laderboard($name,$score)
    {
        $this->load->database();
        $data = array(
            'name' => $name,
            'score' => $score
        );
        $this->db->insert('laderboard', $data);
        $this->db->close();
    }

    public function download_laderboard()
    {
        $this->load->database();
        $this->db->select('name');
        $this->db->order_by("name", "asc");
        $this->db->limit(10);
        $this->db->from('laderboard');
        $data = $this->db->get()->result();
        $this->db->close();
        return $data;
    }

}